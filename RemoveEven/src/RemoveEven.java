import java.util.ArrayList;

public class RemoveEven {
    public static void main(String[] any) {
        int[] myArray = new int[1000000];
        ArrayList<Integer> myArrayList = new ArrayList<>();

        // fill the myArray with consecutive numbers
        for (int i = 0 ; i < myArray.length ; i++ ) {
            myArray[i] = i + 1;
        }

        // Pass myArray values to myArrayList
        for (int num: myArray) {
            myArrayList.add(num);
        }

        long start = System.currentTimeMillis();

        for (int j: juniorDev_filterEven(myArray)) {
            System.out.println(j);
        }
        long finish = System.currentTimeMillis();
        long duration = finish - start; // in milliseconds
        System.out.println("JuniorDev Time: " + duration + " milliseconds");

        long startTime = System.currentTimeMillis();

        for (int k: seniorDev(myArrayList)) {
            System.out.println(k);
        }
        long stopTime = System.currentTimeMillis();
        long totalTime = stopTime - startTime;
        System.out.println("SeniorDev Time: " + totalTime + " milliseconds");
    }

    static int[] juniorDev_filterEven(int[] givenArray) {
        int oddCount = 0;

        // Getting a count of odd numbers
        for (int j : givenArray) {
            if (j % 2 != 0) {
                oddCount++;
            }
        }

        // resultArray of the size of oddCount
        int[] resultArray = new int[oddCount];
        int resultIndex = 0;

        // passing odd numbers into resultArray
        for (int j : givenArray) {
            if (j % 2 != 0) {
                resultArray[resultIndex] = j;
                resultIndex++;
            }
        }
        return resultArray;
    }

    static ArrayList<Integer> seniorDev(ArrayList<Integer> givenArrayList) {
        ArrayList<Integer> resultArray = new ArrayList<Integer>();
        givenArrayList.forEach((num) -> {
            if (num % 2 != 0 ) {
                resultArray.add(num);
            }
        });
        return resultArray;
    }
}
