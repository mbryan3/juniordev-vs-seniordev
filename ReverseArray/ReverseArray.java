import java.util.ArrayList;
import java.util.Collections;

public class ReverseArray {
    public static void main(String[] any) {
        int[] sampleArray = new int[10000000];
        ArrayList<Integer> myArrayList = new ArrayList<Integer>();

        // Initializing sample array with 10 Million elements of consecutive numbers
        for (int i = 0; i < sampleArray.length; i++) {
            sampleArray[i] = i + 1;
            myArrayList.add(sampleArray[i]);
        }

        // juniorDev time analysis
        long start = System.currentTimeMillis();
        printArray(juniorDev_reverseArray(sampleArray));
        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("\n JuniorDev time: " + duration + " milliseconds");

        // seniorDev time analysis
        long begin = System.currentTimeMillis();
        seniorDev_reverseArray(myArrayList).forEach((value) -> {
            System.out.print(value + ", ");
        });
        long stop = System.currentTimeMillis();
        long totalTime = stop - begin;
        System.out.println("\n SeniorDev time: " + totalTime + " milliseconds");
    }

    static void printArray(int[] array) {
        for(int value: array) {
            System.out.print(value + ", ");
        }
    }

    static int[] juniorDev_reverseArray(int[] givenArray) {
        int end = givenArray.length - 1;

        int temp = 0;
        for(int i = 0; i < givenArray.length; i++) {
            temp = givenArray[i];
            givenArray[i] = givenArray[end];
            givenArray[end] = temp;
            end--;
        }

        return givenArray;
    }

    static ArrayList<Integer> seniorDev_reverseArray(ArrayList<Integer> givenArray) {
        Collections.reverse(givenArray);
        return givenArray;
    }
}
