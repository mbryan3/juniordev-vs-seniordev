
public class Sum {
	public static void main(String[] any) {
		long start = System.currentTimeMillis();
		System.out.println(sum_JuniorDev(99999));
		long finish = System.currentTimeMillis();
		long timeElapsed = finish - start;

		System.out.println("JunioDev time: " + timeElapsed + " milliseconds.");

		long startTime = System.currentTimeMillis();
		System.out.println(sum_SeniorDev(99999));
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;

		System.out.println("SeniorDev time: " + totalTime + " milliseconds.");
	}

	static int sum_JuniorDev(int n) {
		int sum = 0;

		for(int i  = 0; i <= n; i++) {
			sum+= i;
		}
		return sum;
	}

	static int sum_SeniorDev(int n) {
		int sum = n * (n + 1) / 2;
		return sum;
	}
}
