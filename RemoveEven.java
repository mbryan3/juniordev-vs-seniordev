// Algorithm to remove even numbers from an array and arrayList
// results showed that seniorDev algorith took less time to execute
// compared to juniorDev
// JunioDev    SeniorDev
//	1548		1480 (with reversing)
//	1548		1455

import java.util.ArrayList;
import java.util.Collections;

public class RemoveEven {
	public static void main(String[] any) {
		int[] myArray = new int[1000000];
		ArrayList<Integer> sampleArrayList = new ArrayList<>();

		// Initializing myArray with consecutive numbers
		for (int i = 0; i < myArray.length ; i++ ) {
			myArray[i] = i + 1;
		}

		// Passing myArray values to the the sampleArrayList
		for (int num: myArray) {
			sampleArrayList.add(num);
		}

		// Measuring time taken by juniorDev
		long start = System.currentTimeMillis();

		// Printing result of juniorDev
		for (int i : junioDev_filterEven(myArray)) {
		 	System.out.println(i);
		 }

		 long finish = System.currentTimeMillis();
		 long totalTime = finish - start;

		 System.out.println("JuniorDev Time: " + totalTime);

		 // Measuring time taken by seniorDev
		 long startTime = System.currentTimeMillis();

		 // Printing result of seniorDev
		 for (int i : seniorDev_filterEven(sampleArrayList)) {
		 	System.out.println(i);
		 }

		 long endTime = System.currentTimeMillis();
		 long total = endTime - startTime;

		 System.out.println("SeniorDev Time: " + total);

	}

	static int[] junioDev_filterEven(int[] anyArray) {
		int indexHolder = 0;

		for (int i = 0 ; i < anyArray.length ; i++ ) {
			if (anyArray[i] % 2 != 0) {
				indexHolder++;
			}
		}

		int[] resultArray = new int[indexHolder];
		int resultIndex = 0;

		for (int i = 0 ; i < anyArray.length ; i++ ) {
			if (anyArray[i] % 2 != 0) {
				resultArray[resultIndex] = anyArray[i];
				resultIndex++;
			}
		}
		return resultArray;
	}

	static ArrayList<Integer> seniorDev_filterEven(ArrayList<Integer> anyArray) {
		ArrayList<Integer> resultArray = new ArrayList<Integer>();

		anyArray.forEach((data) -> {
			if (data % 2 != 0) {
				resultArray.add(data);
			}
		});

		// Not required but simply for fun
		Collections.reverse(resultArray);

		return resultArray;
	}
}
